<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class NamespaceSymfony
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\InterfaceSymfony", mappedBy="namespaceSymfony")
     */
    private $interfacesSymfony;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ClassSymfony", mappedBy="namespaceSymfony")
     */
    private $classesSymfony;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\NamespaceSymfony", inversedBy="child")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\NamespaceSymfony", mappedBy="parent")
     */
    private $child;

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent): void
    {
        $this->parent = $parent;
    }

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $url;

    public function __construct()
    {
        $this->classesSymfony = new ArrayCollection();
        $this->interfacesSymfony = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url): void
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getInterfacesSymfony()
    {
        return $this->interfacesSymfony;
    }

    /**
     * @param mixed $interfacesSymfony
     */
    public function setInterfacesSymfony($interfacesSymfony): void
    {
        $this->interfacesSymfony = $interfacesSymfony;
    }

    /**
     * @return mixed
     */
    public function getClassesSymfony()
    {
        return $this->classesSymfony;
    }

    /**
     * @param mixed $classesSymfony
     */
    public function setClassesSymfony($classesSymfony): void
    {
        $this->classesSymfony = $classesSymfony;
    }
}
