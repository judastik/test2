<?php

namespace App\Controller;

//неймспейсы украл из примера
use App\Entity\Article;
use App\Form\ArticleType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController.
 */
class DefaultController extends AbstractController
{
    /** @var EntityManagerInterface */
    private $em;

    /**
     * DefaultController constructor.
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/", name = "article_read")
     *
     * @Route("/article/{_locale}", name="article_read", requirements={"_locale"="en|ru"})
     *
     * @return Response
     */
    public function readAction()
    {
        $em = $this->getDoctrine()->getManager();
        $articles = $em->getRepository(Article::class)->findAll();

        return $this->render('article/read.html.twig', ['articles' => $articles]);
    }

    /**
     * @Route("/create", name="article_create")
     *
     * @Route("/article/{_locale}/create", name="article_create", requirements={"_locale"="en|ru"})
     *
     * @return Response
     */
    public function createAction(Request $request)
    {
        $article = new Article();
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($article);
            $em->flush();

            return $this->redirectToRoute('article_read');
        }

        return $this->render('article/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/update/{id}", name = "article_update")
     *
     * @Route("/article/{_locale}/update/{id}", name="article_update", requirements={"_locale"="en|ru"})
     *
     * @return Response
     */
    public function updateAction(int $id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository(Article::class)->find($id);


        if (!$article) {
            throw $this->createNotFoundException('Article with ID'.$id.'not found!');
        }

        $editForm = $this->createForm(ArticleType::class, $article);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $this->em->persist($article);

            $this->em->flush();

            return $this->redirectToRoute('article_read', ['id' => $id]);
        }

        return $this->render('article/update.html.twig', ['article' => $article, 'editForm' => $editForm->createView()]);
    }

    /**
     * @Route("/delete/{id}", name = "article_delete")
     *
     * @Route("/article/{_locale}/delete/{id}", name="article_delete", requirements={"_locale"="en|ru"})
     *
     * @return Response
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository(Article::class)->find($id);

        if (!$article) {
            throw $this->createNotFoundException('Article with ID'.$id.'not found!');
        }

        $this->em->remove($article);
        $this->em->flush();

        return $this->redirectToRoute('article_read');
    }

    /**
     * @Route("/article/locale/{locale}", name="article_locale")
     */
    public function changeLocaleAction(Request $request, string $locale): Response
    {
        $request->getSession()->set('_locale', $locale);

        return $this->redirectToRoute('article_read');
    }
}
